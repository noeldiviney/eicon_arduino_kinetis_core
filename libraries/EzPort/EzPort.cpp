/*
 *
 *
 */
#include <SPI.h>

#include "EzPort.h"

bool EzPort::select(uint8_t fast)
{
	if((cst&3)!=1) return false; // if the connection is not 'open' or the connection is already 'select'ed then don't touch it.
	digitalWrite(_slaveSelectPin, LOW);
	delay(1);
	if(fast)
	{
		SPI.beginTransaction(SPISettings(4000000, MSBFIRST, SPI_MODE0));
	} else {
		SPI.beginTransaction(SPISettings(1000000, MSBFIRST, SPI_MODE0));
	} 
	cst|=2;
	return true;
}

void EzPort::unselect()
{
	if((cst&3)!=3) return; // if the connection is not 'open' or not 'select'ed then don't touch it.
	SPI.endTransaction();
	digitalWrite(_slaveSelectPin, HIGH);
	cst&=~2;
	delay(1); // give it a millisec.
//	Serial.printf("done unselect()\n");
}

void EzPort::open()
{
    if(cst&2) unselect();
	if(cst) close();
	pinMode(_resetPin, OUTPUT);
    pinMode(_slaveSelectPin, OUTPUT);
    digitalWrite(_resetPin, LOW);
    digitalWrite(_slaveSelectPin, LOW);
    delay(50);
    digitalWrite(_resetPin, HIGH);
    delay(100);
	digitalWrite(_slaveSelectPin, HIGH);
	cst|=1;
}

void EzPort::close()
{
	if(cst&2) unselect();
	digitalWrite(_resetPin,LOW);
	pinMode(_slaveSelectPin, INPUT_PULLUP);
	delay(50);
	pinMode(_resetPin, INPUT_PULLUP);
	cst&=~1;
}

uint8_t EzPort::readStatus()
{
	if(!select()) return 0xfd;
	SPI.transfer(RDSR);
	uint8_t temp=SPI.transfer(0);
	unselect();
	return(temp);
}

bool EzPort::writeEnable(bool en)
{
	if(!select()) return false;
	if(en) SPI.transfer(WREN); else SPI.transfer(WRDI);
	unselect();
	return ((readStatus() & (WEN|BEDIS|FS|WEF))==WEN);
}

void EzPort::read(uint32_t address, uint8_t* ptr)
{
	uint8_t addr1, addr2, addr3;

	addr3 = address & 0xff;
	addr2 = address >> 0x08 & 0xff;
	addr1 = address >> 0x10 & 0xff;

	if(!select()) return;
	
	SPI.transfer(READ);
	SPI.transfer(addr1);
	SPI.transfer(addr2);
	SPI.transfer(addr3);
	
	*ptr++=SPI.transfer(0);
	*ptr++=SPI.transfer(0);
	*ptr++=SPI.transfer(0);
	*ptr=SPI.transfer(0);
	
	unselect();
	
}

bool EzPort::bulkErase()
{
	if(!writeEnable()) return(false);
	uint8_t n=readStatus();
	if((!n&WEN)||(n&(WEF|BEDIS|FS|FLEXRAM))) return(false);
	
	if(!select()) return(false);
	SPI.transfer(BE);
	unselect();
	n=readStatus()|WIP; // set it up so WIP looks active at this point in time...
	while((n&(WIP|WEF))==WIP) n=readStatus(); // wait for command complete.
	if(n&WEF) return(false);
	return(true);
}

bool EzPort::sectorErase(uint32_t address)
{
	uint8_t addr1, addr2, addr3;

	writeEnable();
	uint8_t n=readStatus();
	if((!n&WEN)||(n&(WEF|BEDIS|FS|FLEXRAM))) return(false);
	
	addr3 = address & 0xff;
	addr2 = address >> 0x08 & 0xff;
	addr1 = address >> 0x10 & 0xff;

	if(!select()) return(false);
	SPI.transfer(SE);
	SPI.transfer(addr1);
	SPI.transfer(addr2);
	SPI.transfer(addr3);
	unselect();
	n=readStatus();
	while((n&(WIP|WEF))==WIP) n=readStatus(); // wait for command complete.
	if(n&WEF) return(false);
	return(true);
}



uint16_t EzPort::write(uint32_t address, uint8_t* buffer, uint16_t length) // non-zero return means error occured.
{
	uint8_t addr1, addr2, addr3,n=0;
	uint16_t byteCount=0,byteCount1=0,switchCount=0;
	uint32_t switchRestart=0;

	if((address<0x40C)&&(address+length>=0x40C))
	{
		uint16_t p=0x40C-(uint16_t)address;
		buffer[p]=0xFE; // no locking it via programmer, thanks.
	}
	if((address<0x40D)&&(address+length>=0x40D))
	{
		uint16_t p=0x40D-(uint16_t)address;
		buffer[p]=0xFF; // no locking out EZPORT via programmer, thanks.
	}

	addr3 = address & 0xff;
	addr2 = address >> 0x08 & 0xff;
	addr1 = address >> 0x10 & 0xff;

	if(_writing_caveat)
	{
		switchCount=length-_writing_caveat;
		switchRestart=address+length-_writing_caveat;
	} else {
		switchCount=length+1;
	}

	if(!writeEnable()) return(256|readStatus()); // first attempt to write-enable
	if(!select()) return(512|readStatus()); // first attempt to select
	
	SPI.transfer(SP);
	SPI.transfer(addr1);
	SPI.transfer(addr2);
	SPI.transfer(addr3);
	
	while(byteCount<length)
	{
		SPI.transfer(*buffer++);
		byteCount++;
		if(_writing_caveat)
		{
			byteCount1++;
			if(byteCount>switchCount-1) // really prefer 'not bigger than...'
			{
				while(byteCount1<_writing_caveat)
				{
					SPI.transfer(0xff);
					byteCount1++;
				}
			}
			if(byteCount1>_writing_caveat-1)
			{
				unselect();
				n=readStatus()|WIP;
				while((n&(WIP|WEF))==WIP) n=readStatus(); // wait for command complete.
				if((n&WEF)==WEF) return(1024|n); // This instance of error flag.

				byteCount1=0;
				
				if(byteCount<length)
				{
				
					if(byteCount<switchCount)
					{
						address+=_writing_caveat;
					} else {
						switchCount=length+1; // don't accidentally switch again, that wouldn't be cool.
						address=switchRestart;
					}

					if(!writeEnable()) return(2048|readStatus());
					if(!select()) return(4096|readStatus());
					
					addr3 = address & 0xff;
					addr2 = address >> 0x08 & 0xff;
					addr1 = address >> 0x10 & 0xff;
					SPI.transfer(SP);
					SPI.transfer(addr1);
					SPI.transfer(addr2);
					SPI.transfer(addr3);
				}
			}
		}
	}

	if(cst&2)
	{
		unselect();
		n=readStatus()|WIP;
		while((n&(WIP|WEF))==WIP) n=readStatus(); // wait for command complete.
		if(n&WEF) return(8192|n);
	}
	writeEnable(false);
	return(0);
}

