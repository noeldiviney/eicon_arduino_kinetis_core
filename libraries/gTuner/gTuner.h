



#ifndef gTuner_h
	#define gTuner_h
	#ifdef __cplusplus

		#include <inttypes.h>
		#include "Arduino.h"

		

class gTuner
{
  public:
	gTuner() { begin(); }
	void inSample(void);
	uint16_t update(void);
	float detectedFreq;
	float lastDiff;
	void Tolerance(double val);
	
	uint8_t flags;
	float tuning_scope=0.01; // was dividing, going to multiply now, 1% of detectedAverage might be the right scope.
	uint8_t minSamplePairs=3;
	uint8_t minMatchedSamples=2;
	
  protected:
	
	
  private:
	void begin(void);
	const double notes[12]={16.3515978312874,17.3239144360545,18.354047994838,19.4454364826301,20.6017223070544,21.8267644645627,
		23.1246514194771,24.4997147488593,25.9565435987466,27.5,29.1352350948806,30.8677063285078};
	// ratios normalising around notes[6] (where ratios[n]=notes[n]/notes[6])
	const double ratios[12]={1.41421356237309,1.33483985417003,1.25992104989487,1.18920711500272,1.12246204830937,1.0594630943593,
		1,0.943874312681693,0.890898718140336,0.840896415253713,0.793700525984099,0.749153538438338};
	uint8_t myDir; 
	int32_t lastSample[2]={0,0};
	uint16_t myAnswer=(0x7<<8)|12; // leds & segs combined in 16 bits.
	int32_t inSamples[2];
	int32_t samples[2][8];
	int32_t accum[2][4];
	int32_t acc0[12];
	uint8_t sptr[2];
	uint8_t aDir;
	uint8_t cStep=0;
	double nTol[12];
	double minFreq=30;
	
	elapsedMicros gTimer;
	
	double runningAvg=50000;
	double addAvgs[4]={1000000,1000000,1000000,1000000};
	uint8_t aptr=0;
	double detectedAvg;
	double lastAvg;
	elapsedMillis dumpit;
	
	
//	double savgs[2][3];
	
};



	#endif // __cplusplus
#endif // gTuner_h
