
#include "muxer.h"

// Hey, you need to rewrite this in light of how it uses bad control_words....

void AudioMuxer43::update(void)
{
	audio_block_t *in[4], *out=NULL;
	int16_t *ptr1=NULL,*ptr2=NULL,*ptr3=NULL;
	uint8_t blank_state=255,nn=0;

	in[0]=receiveReadOnly(0);
	in[1]=receiveReadOnly(1);
	in[2]=receiveReadOnly(2);
	in[3]=receiveReadOnly(3);
	out=allocate();
	
	nn=(control_words[0]&(3<<16))>>16; // the selected input
	if(!(control_words[0]&(1<<20))||(!in[nn])) // output is disabled or no data arrived for it.
	{
		if(blank_state)
		{
			if(out)
			{
				ptr1=out->data;
				ptr3=ptr1+AUDIO_BLOCK_SAMPLES;
				do { *(ptr1++)=0; } while(ptr1<ptr3);
				blank_state=0;
				transmit(out,0);
			}
		}
	} else if(control_words[0]&(1<<19)) {// output 0 is inverted.
		if(out)
		{
			ptr1=in[nn]->data;
			ptr2=out->data;
			ptr3=ptr1+AUDIO_BLOCK_SAMPLES;
			do { *(ptr2++)=-(*(ptr1++)); } while(ptr1<ptr3);
			blank_state=1; // somewhat messy
			transmit(out,0);
		} else { // we had no out but we have the 'in' so we will just send it uninverted.
			transmit(in[nn],0);
		}
	} else {
		transmit(in[nn],0);
	}
	
	nn=(control_words[1]&(3<<16))>>16;
	if(!(control_words[1]&(1<<20))||(!in[nn]))
	{
		if(blank_state)
		{
			if(out)
			{
				ptr1=out->data;
				ptr3=ptr1+AUDIO_BLOCK_SAMPLES;
				do { *(ptr1++)=0; } while(ptr1<ptr3);
				blank_state=0;
			}
		}
		transmit(out,1);
	} else if(control_words[1]&(1<<19)) {
		if(out)
		{
			ptr1=in[nn]->data;
			ptr2=out->data;
			ptr3=ptr1+AUDIO_BLOCK_SAMPLES;
			do { *(ptr2++)=-(*(ptr1++)); } while(ptr1<ptr3);
			blank_state=1;
			transmit(out,1);
		} else {
			transmit(in[nn],1);
		}
	} else {
		transmit(in[nn],1);
	}
	
	nn=(control_words[2]&(3<<16))>>16;
	if(!(control_words[2]&(1<<20))||(!in[nn]))
	{
		if(blank_state)
		{
			if(out)
			{
				ptr1=out->data;
				ptr3=ptr1+AUDIO_BLOCK_SAMPLES;
				do { *(ptr1++)=0; } while(ptr1<ptr3);
			}
		}
		transmit(out,2);
	} else if(control_words[2]&(1<<19)) {
		if(out)
		{
			ptr1=in[nn]->data;
			ptr2=out->data;
			ptr3=ptr1+AUDIO_BLOCK_SAMPLES;
			do { *(ptr2++)=-(*(ptr1++)); } while(ptr1<ptr3);
			transmit(out,2);
		} else { 
			transmit(in[nn],2);
		}
	} else {
		transmit(in[nn],2);
	}
	if(in[0]) release(in[0]);
	if(in[1]) release(in[1]);
	if(in[2]) release(in[2]);
	if(in[3]) release(in[3]);
	if(out) release(out);
}
