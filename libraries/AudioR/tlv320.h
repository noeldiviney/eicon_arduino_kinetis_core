
#define TLV320xxx_I2C 0x18

#include "AudioControl.h"

// class AudioControlSGTL5000 : public AudioControl

class AudioControlTLV320 : public AudioControl
{

public:
	unsigned char writeReg(unsigned char page, unsigned char reg, unsigned char val);
	unsigned char readReg(unsigned char page, unsigned char reg);
	bool enable(void);
	// bool enableSlave(void);
//	bool enableSlave(uint8_t decimation=8, uint8_t interpolation=8);
	bool enableSlave(uint8_t reset_pin, uint8_t decimation=8, uint8_t interpolation=8);
	bool disable(void) { return false; }
	bool volume(float left, float right);
	bool volume(float n) { return volume(n,n); }
	
	bool inputLevel(float n) { return dBlineInAdjust(n*20,n*20); }
	bool inputSelect(int n) {return false; }
	
	void routeIn1(unsigned char left, unsigned char right);
	void routeIn1(unsigned char both) { routeIn1(both,both); }
	void routeIn2(unsigned char left, unsigned char right);
	void routeIn2(unsigned char both) { routeIn2(both,both); }
	void routeIn3(unsigned char left, unsigned char right);
	void routeIn3(unsigned char both) { routeIn3(both,both); }

	
	
	unsigned char lineOutMute(bool left=true, bool right=true);
	unsigned char lineOutMute(bool n) { return lineOutMute(n,n); }
	// unsigned char lineOutMute(void) { return lineOutMute(true,true); }
	unsigned char lineOutUnMute(void) { return lineOutMute(false,false); }
	
	unsigned char headPhoneMute(bool left=true, bool right=true);
	unsigned char headPhoneMute(bool n) { return headPhoneMute(n,n); }
	// unsigned char headPhoneMute(void) { return headPhoneMute(true,true); }
	unsigned char headPhoneUnMute(void) { return headPhoneMute(false,false); }
	
	unsigned char lineOutEn(bool nl, bool nr);
	unsigned char lineOutEn(bool n) { return lineOutEn(n,n); }
	
	unsigned char dacOutEn(bool nl, bool nr);
	unsigned char dacOutEn(bool n) { return lineOutEn(n,n); }
	
	unsigned char headPhoneEn(bool nl, bool nr);
	unsigned char headPhoneEn(bool n) { return headPhoneEn(n,n); }
	
	unsigned char dBlineInAdjust(float left, float right); // valid values -12 to +20 in 0.5dB steps
	unsigned char dBlineInAdjust(float n) { return dBlineInAdjust(n,n); }
	
	unsigned char dBadjustDAC(float left, float right); // valid values -63.5 to 24 in 0.5dB steps.
	unsigned char dBadjustDAC(float n) { return dBadjustDAC(n,n); }
	
	unsigned char dBlineOutAdjust(float left, float right); // valid values -6 to +29 in 1dB steps
	unsigned char dBlineOutAdjust(float n) { return dBlineOutAdjust(n,n); }
	
	unsigned char dBheadPhoneAdjust(float left, float right); // valid values -6 to +29 in 1dB steps
	unsigned char dBheadPhoneAdjust(float n) { return dBheadPhoneAdjust(n,n); }
	
	unsigned char dBmixerGain(float left, float right); // valid values 0 to 47.5 in 0.5dB steps
	unsigned char dBmixerGain(float n) { return dBmixerGain(n,n); }
	
	unsigned char AGCenable(bool nl, bool nr);
	
	bool AGCenabled(uint8_t channel);
	unsigned char AGCenabled(uint8_t channel, bool setting);
	
	unsigned char AGCdBtarget(uint8_t channel); // returns the current target in the given channel.
	unsigned char AGCdBtarget(uint8_t channel, uint8_t target); // sets the current target
	
	unsigned char AGCdBgainHysteresis(uint8_t channel);
	unsigned char AGCdBgainHysteresis(uint8_t channel, uint8_t gainHyst);
	
	unsigned char AGCdBhysteresis(uint8_t channel);
	unsigned char AGCdBhysteresis(uint8_t channel, uint8_t hyst);
	
	unsigned char AGCdBnoiseThreshold(uint8_t channel);
	unsigned char AGCdBnoiseThreshold(uint8_t channel, uint8_t thresh);
	
	unsigned char AGCdBmaxGain(uint8_t channel);
	unsigned char AGCdBmaxGain(uint8_t channel, uint8_t gain);
	
	unsigned char AGCattackSetting(uint8_t channel);
	unsigned char AGCattackSetting(uint8_t channel, uint8_t setting);
	
	unsigned char AGCattackScale(uint8_t channel);
	unsigned char AGCattackScale(uint8_t channel, uint8_t setting);
	
	unsigned char AGCdecaySetting(uint8_t channel);
	unsigned char AGCdecaySetting(uint8_t channel, uint8_t setting);
	
	unsigned char AGCdecayScale(uint8_t channel);
	unsigned char AGCdecayScale(uint8_t channel, uint8_t setting);
	
	unsigned char AGCnoiseDebounce(uint8_t channel);
	unsigned char AGCnoiseDebounce(uint8_t channel, uint8_t setting);
	
	unsigned char AGCsignalDebounce(uint8_t channel);
	unsigned char AGCsignalDebounce(uint8_t channel, uint8_t setting);

	int8_t AGCcurrentGain(uint8_t channel);

	bool DRCenabled(uint8_t channel);
	unsigned char DRCenabled(uint8_t channel, bool setting);

	unsigned char DRCthresholdControl();
	unsigned char DRCthresholdControl(uint8_t setting);

	unsigned char DRChysteresisControl();
	unsigned char DRChysteresisControl(uint8_t setting);
	
	unsigned char DRChold();
	unsigned char DRChold(uint8_t setting);
	
	unsigned char DRCattackRate();
	unsigned char DRCattackRate(uint8_t setting);
	
	unsigned char DRCdecayRate();
	unsigned char DRCdecayRate(uint8_t setting);
	
	
	void biquadADC(unsigned char channel, unsigned char set, int const coefs[]);
	void biquadDAC(unsigned char channel, unsigned char set, int const coefs[]);

protected:
	
private:
	void chkPage(unsigned char page);
	void biquadCopyADC(unsigned char channel, unsigned char set);
	void biquadCopyDAC(unsigned char channel, unsigned char set);
	void biquadSwitchADC(void);
	void biquadSwitchDAC(void);

//	unsigned char calcVol(float n, unsigned char range);
	uint8_t curPage=0;
	
};

/************ Special function models ***********/
bool setI2SmasterClock(uint32_t freq, uint32_t mult = 256);

