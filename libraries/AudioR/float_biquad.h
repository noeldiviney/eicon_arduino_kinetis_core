#ifndef filter_biquad_h_
#define filter_biquad_h_

#include "Arduino.h"
#include "AudioStream48.h"

class AudioFloatBiquad : public AudioStream
{
public:
	AudioFloatBiquad(float* defs) : AudioStream(1, inputQueueArray) {
		definition=defs;
	}
	virtual void update(void);
	
private:
	audio_block_t *inputQueueArray[1];
	float* definition=NULL; // 
};

#endif

