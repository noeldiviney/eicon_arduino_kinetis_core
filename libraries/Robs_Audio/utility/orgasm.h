
#ifndef asm_biquad_h_
#define asm_biquad_h_

#ifdef __cplusplus
extern "C" {
#endif

void gasmInt2Float(int16_t* ptr16, float* ptrf);
void gasmInt2FloatPeak(int16_t* ptr16, float* ptrf, float* peaks);
void gasmFloat2Int(float* ptrf, int16_t* ptr16);
void gasmFloat2IntGain(float* ptrf, int16_t* ptr16, float* gain);
void gasmFloat2IntGainPeak(float* ptrf, int16_t* ptr16, float* gain, float* peak);
void gasmBiquadf(float* ptrf, float* coefs);
void gasmBiquadff(float* dest, float* coefs, float* src);
void gasmBiquadffPeak(float* dest, float* coefs, float* src, float* peaks);
void gasmMix2(float* buff0, float* buff1, float* gain);

void gasmCaterPeaksAndCounters(float* peakinfo, uint8_t* counterinfo);

#ifdef __cplusplus
}
#endif


#endif
