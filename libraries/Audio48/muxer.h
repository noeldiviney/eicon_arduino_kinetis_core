// propose object to accept 4 inputs and offer 3 outputs

// each of the three outputs capable of selecting one input at a time with ability to invert as needed.


#ifndef muxer_h_
#define muxer_h_

#include "Arduino.h"
#include "AudioStream48.h"

class AudioMuxer43 : public AudioStream
{
public:
    AudioMuxer43(uint32_t * ptr) : AudioStream(4, inputQueueArray) {
		control_words=ptr;
	}
    AudioMuxer43(void) : AudioStream(4, inputQueueArray) {
		control_words=NULL;
	}
    virtual void update(void);
	void mux(uint8_t n, uint8_t s) {
		if(s>3)
		{
			control_words[n]&=~(31<<16);
			control_words[n]|=(s<<16);
		} else {
			control_words[n]&=~(3<<16);
			control_words[n]|=(s<<16);
		}
	}
	void enabled(uint8_t n, bool s) {if(s) control_words[n]|=(1<<20); else control_words[n]&=~(1<<20);}
	void inverted(uint8_t n, bool s) {if(s) control_words[n]|=(1<<19); else control_words[n]&=~(1<<19);}
	
	void pointer(uint32_t * ptr) { control_words=ptr; } // the control_words is an array in the main register file.
	
private:
	audio_block_t *inputQueueArray[4];
	uint32_t * control_words;
};

#endif
